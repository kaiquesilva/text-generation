import re
import time
import numpy as np
import tensorflow as tf

from inheriting_modeling import build_component

from composable import mapping_indices_unique
from composable import read_file_content

tf.enable_eager_execution()

# data = read_file_content() # default content settled for shakespheare
data = read_file_content('le-kama-soutra', 'http://www.gutenberg.org/cache/epub/14609/pg14609.txt')

char2idx, idx2char, unique_data = mapping_indices_unique(data)

max_length = 100
vocab_size = len(unique_data)
embedding_dim = 256
units = 1024

BATCH_SIZE = 64
BUFFER_SIZE = 10000

input_text, target_text = [], []

starting_point = 0
text_size_limit = len(data) - max_length
aranged_dimensions = np.arange(starting_point, text_size_limit,
							   max_length)

for f in aranged_dimensions:
	dimension_reductionalities = f + max_length
	inputs = data[f:dimension_reductionalities]
	dimension_strengths = f + 1
	dimension_strengths_reductionalities = dimension_strengths + max_length
	targets = data[dimension_strengths:dimension_strengths_reductionalities]
	input_text.append([char2idx[i] for i in inputs])
	target_text.append([char2idx[t] for t in targets])

input_text_array = np.array(input_text)
target_text_array = np.array(target_text)

print('Input text shape: {}'.format(input_text_array.shape))
print('Target text shape: {}'.format(target_text_array.shape))

InheritedDataset = build_component(tf.data.Dataset)

text_elements = (input_text, target_text)
dataset = InheritedDataset \
			.from_tensor_slices(text_elements) \
			.shuffle(BUFFER_SIZE) \
			.apply(tf.contrib.data.batch_and_drop_remainder(BATCH_SIZE))

with tf.variable_scope('Model with optimizer declarations') as scope:
	InheritedModel = build_component(tf.keras.Model)
	InheritedEmbedding = build_component(tf.keras.layers.Embedding)
	InheritedDense = build_component(tf.keras.layers.Dense)

	class GenerationModel(InheritedModel):
		def __init__(self, *args, **kwargs):
			super(GenerationModel, self).__init__(*args, **kwargs)

			vocab_size = args[0]
			embedding_dim = args[1]
			units = args[2]
			SCOPED_BATCH_SIZE = args[3]

			self.units = units
			self.batching_size = SCOPED_BATCH_SIZE

			self.embedding = InheritedEmbedding(vocab_size,
													   embedding_dim)

			self.initialize_gru()

			self.fc = InheritedDense(vocab_size)

		def initialize_gru(self):
			if self.is_gpu_available():
				self.gru = self.build_gpu_layer()
			else:
				self.gru = self.build_gru_layer()

		def is_gpu_available(self):
			return tf.test.is_gpu_available()

		def build_gpu_layer(self):
			InheritedCuDNNGRU = build_component(tf.keras.layers.CuDNNGRU)
			return InheritedCuDNNGRU(self.units,
									        return_sequences=True,
									        return_state=True,
									        recurrent_initializer='glorot_uniform')

		def build_gru_layer(self):
			InheritedGRU = build_component(tf.keras.layers.GRU)
			return InheritedGRU(self.units,
											return_sequences=True,
											return_state=True,
											recurrent_activation='sigmoid',
											recurrent_initializer='glorot_uniform')

		def call(self, x, hidden):
			x = self.embedding(x)
			output, states = self.gru(x, initial_state=hidden)
			output = tf.reshape(output, (-1, output.shape[2]))
			x = self.fc(output)
			return x, states

	InheritedGenerationModel = build_component(GenerationModel)
	model = InheritedGenerationModel(vocab_size, embedding_dim, units, BATCH_SIZE)
	InheritedAdamOptimizer = build_component(tf.train.AdamOptimizer)
	optimizer = InheritedAdamOptimizer()

with tf.name_scope('loss feature definition') as scope:
	def loss_fn(real, preds):
		return tf.losses.sparse_softmax_cross_entropy(labels=real, logits=preds)

void = lambda s=int(): chr(s)

with tf.name_scope('timeless iteration feature definition') as scope:
	def print_epoch_batch_loss_fn(epoch, batch, loss):
		if batch % 100 == 0:
			print('Epoch {} Batch {} Loss {:.4f}'.format(epoch+1, batch, loss))
		return void()

	EPOCHS = 30

with tf.name_scope('learning hate dataset proportional feature definition') as scope:
	def learn_from_dataset_fn(hidden):
		with tf.name_scope('loss') as scope:
			InheritedGratientTape = build_component(tf.GradientTape)
			for (batch, (inp, target)) in enumerate(dataset):
				with InheritedGratientTape() as tape:
					predictions, hidden = model(inp, hidden)
					target = tf.reshape(target, (-1,))
					loss = loss_fn(target, predictions)
				grads = tape.gradient(loss, model.variables)
				optimizer.apply_gradients(zip(grads, model.variables),
										  global_step=tf.train.get_or_create_global_step())
				print_epoch_batch_loss_fn(epoch, batch, loss)
		return (epoch, loss)

with tf.name_scope('learning_process') as scope:
	for epoch in np.arange(EPOCHS):
		start = time.time()
		hidden = model.reset_states()
		epoch, loss = learn_from_dataset_fn(hidden)
		print('Epoch {} Loss {:.4f}'.format(epoch+1, loss))
		print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))

with tf.name_scope('generation_process') as scope:
	num_generate = 1000
	start_string = 'Q'
	input_eval = [char2idx[s] for s in start_string]
	input_eval = tf.expand_dims(input_eval, 0, name='input_eval')
	text_generated = ''
	temperature = 1.0
	hidden = [tf.zeros((1, units), name='hidden_units')]

	for i in np.arange(num_generate):
		predictions, hidden = model(input_eval, hidden)
		predictions = predictions / temperature
		predicted_id = tf.multinomial(tf.exp(predictions, name='expressed_predictions'),
									  num_samples=1, name='predicted_identification')[0][0].numpy()
		input_eval = tf.expand_dims([predicted_id], 0, name='input_evaluation_expanded_dimensions')
		text_generated += idx2char[predicted_id]

	print(start_string + text_generated)