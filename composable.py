import tensorflow as tf
from unidecode import unidecode

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--retext', default='ref', type=str,
								help='Kind of usage based on a outage context')
parser.add_argument('--filename', default='shakespeare.txt', type=str,
								  help='File name of url to downloaded')
parser.add_argument('--url', default='https://storage.googleapis.com/yashkatariya/shakespeare.txt', type=str,
							 help='Url to be produced by content retrieval')

def grab_file_content(filename='shakespeare.txt',
					  url='https://storage.googleapis.com/yashkatariya/shakespeare.txt'):
	return tf.keras.utils.get_file(filename, url)

def mapping_indices(refs):
	return ({u:i for i, u in enumerate(refs)},
			{i:u for i, u in enumerate(refs)})

def read_file_content(filename=None, url=None, file_path=grab_file_content()):
	if filename and url:
		file_path = grab_file_content(filename, url)

	with open(file_path) as f:
		data = unidecode(f.read())
		f.close()
	return data

def mapping_indices_unique(data):
	unique_data = sorted(set(data))
	char2idx, idx2char = mapping_indices(unique_data)
	return (char2idx, idx2char, unique_data)

def main(argv):
	args = parser.parse_args(argv[1:])
	
	if args.retext == 'book':
		grab_file_content(args.filename, args.url)

	parser.print_help()

if __name__ == '__main__':
	tf.logging.set_verbosity(tf.logging.INFO)
	tf.app.run(main)