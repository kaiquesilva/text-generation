import tensorflow as tf

from composable import mapping_indices

class MappingIndicesTestCase(tf.test.TestCase):
	def testImutableSequenceExistence(self):
		with self.test_session():
			data = 'some data relation in test content'
			expectation = len(mapping_indices(data)) > 0
			expected = True
			self.assertEqual(expectation, expected)

	def testImutableSequenceParentType(self):
		with self.test_session():
			data = 'some data relation in test content'
			expectation = mapping_indices(data)
			expectation = [type(item) == dict for item in expectation]
			expected = [True, True]
			self.assertEqual(expectation, expected)

if __name__ == '__main__':
	tf.test.main()